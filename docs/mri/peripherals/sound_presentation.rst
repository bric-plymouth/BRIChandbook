.. _sound_presentation:

Sound presentation instructions and notes
-----------------------------------------

The full user guide for the MR Confon BOLDfonic sound presentation system is available here.
:download:`BOLDfonic_MR Confon_User_Guide </mri/peripherals/071-CRS-0122_R05_MR_CONFON_Starter_f_MKII+_User_Guide__for_email_.pdf>`


Quick instructions for using the BOLDfonic audio system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

These instructions assume that you are presenting sounds using the Windows desktop computer in the MRI control room.

- Make sure that the MKII+ amplifier is turned ON (power button at the back)
- Make sure that the MKII+ amplifier is using correct source: the right-side LCD shows "IN-STIM" as the source (corresponding to "fMRI" mode). If not, press the start button twice on the front side of the amplifier, or until "IN-STIM" shows.

.. figure:: /mri/peripherals/Starter_f_MKII+_Amplifier.png
   :alt: Starter f MKII+ Amplifier
   :width: 400px
   :align: center

   Starter f MKII+ Amplifier

- Sound volume can be controlled at three levels:
    - the intensity level of your sound files. This should ideally be maximised beforehand so that your sounds use the largest amplitude range possible.
    - the output level of the sound card on the stimulus presentation computer. This can be set through the Windows OS (e.g. bottom right of the task bar).
    - the output level of the MKII+ amplifier, which can be set between -24 and +24 dB, relative to the input from the presentation computer, using the central dial.
- Once you have chosen the sound level to use in your experiment, make a note of the sound levels on both the stimulus presentation computer and the MKII+ amplifier. Make sure you set the correct levels for each participant.
- It is not possible to know the sound level of the presented stimuli in dB SPL unless the entire sound system has been calibrated. This is a complex process that requires additional equipment. Speak to the lab directors if this is something you need.

.. figure:: /mri/peripherals/Desktop_monitor_speaker.png
   :alt: Desktop monitor speaker
   :width: 250px
   :align: right

   Desktop monitor speaker

- Sound presentation can be monitored in the control room using the desktop monitor speaker.
- To control the monitor speaker's volume:
    - Push the amplifier's Volume dial once (the left LCD changes from "MUSIC-VOL" to "MUSIC-MONVL" on the first line and from "Balance" to "SUBJ-MONVL" on the second line).
    - SUBJ-MONVL is the Desktop Monitor speaker volume and can be adjusted using the Balance dial.

- Sounds can be presented to the participants using either ultra-slim piezo-electric headphones or electrodynamic earphones

.. figure:: /mri/peripherals/HP_AT_01_earphones.png
   :alt: HP AT 01 electrodynamic earphones
   :height: 250px
   :align: right

   HP AT 01 electrodynamic earphones

.. figure:: /mri/peripherals/HP_PI_US_03_headphones.png
   :alt: HP PI US 03 piezo-electric headhpones
   :height: 250px
   :align: right

   HP PI US 03 piezo-electric headphones

- Piezo-electric headphones work both inside and outside the MRI room. Electro-dynamic earphones need to be within the scanner's magnetic field to function. The earphones drivers (white cubes) must be placed just outside the head coil.
- For both headphones and earphones, the right-ear side is marked in red.

.. figure:: /mri/peripherals/HP_AT_01_earplug_with_defender.png
   :alt: HP AT 01 earplug with ear defender
   :width: 230px
   :align: right

   HP AT 01 earplug with ear defender

- Both headphones and earphones provide some noise attenuation, but additional noise protection should be provided:

    - When using the headphones, the participant should wear earplugs within them.
    - The insert earphones should be worn within the provided ear defenders.


- The BOLDfonic system also allows one-way communication from the console room to the participant using the MR Confon's technologist microphone. When using the BOLDfonic system, this microphone replaces the Siemens technologist microphone. Communication from the participant to the console room always uses the Siemens system.

.. figure:: /mri/peripherals/Technologist_microphone.png
   :alt: MR Confon Technologist microphone
   :width: 230px
   :align: right

   MR Confon Technologist microphone


- To talk to the participant, press the "Talk" button on the microphone's base. This will override any audio stimulus being presented.
- To set the microphone level:

    - press the "Talk" button on the microhpone
    - use the Volume dial on the MKII++ amplifier



Which earphones/headphones should I use?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You should consider the following when choosing between headphones and earphones:

- **Sound level**: Because participant should wear earplugs within the headphones, this limits the effective intensity level at which auditory stimuli can be presented. Depending on the stimuli used, the maximum volume allowed by the stimulus presentation computer and amplifier may be insufficient for the stimuli to be audible or intelligible through the ear plugs. With the insert earphones, on the other hand, sound is delivered by a tube going through the ear plug, and therefore higher intensity levels can be reached at the participant's ear.
- **Distortions**: because they carry sounds through narrow tubes, the earphones are likely to introduce larger spectral distortions in the stimuli than the headphones.
- **Testing outside the scanner**: unlike the earphones, the headphones can work outside the scanner room and are therefore easier to test or calibrate.

