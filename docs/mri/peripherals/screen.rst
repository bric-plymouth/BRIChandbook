.. _screen:

Screen instructions and notes
-----------------------------

The Getting Started guide is available here.
:download:`BOLDScreen32_Getting_Started </mri/peripherals/071-CRS-0154_BOLDscreen32AVI_Getting_Started_Guide.pdf>`

The following notes assume that you are presenting visual stimuli using the Windows desktop computer in the MRI control room.


Windows screen settings
~~~~~~~~~~~~~~~~~~~~~~~

By default, the screen in the MRI room should mirror the right-hand-side monitor in the control room, while
the left-hand side monitor should not be seen by the participant in the scanner.

To achieve this, ensure the following screen settings are set on the Windows desktop computer:

- right-click on the desktop and click "Display settings"
- in "Rearrange your displays", select display #1, #2 or 1|2
- in "Multiple Displays" at the bottom of the Display settings page:
    
    - select "Duplicate desktop on 1 and 2
    - check "Make this my main display"
    
- in "Rearrange your displays" at the top, select display #3
- in "Multiple Displays" at the bottom of the Display settings page, select "Extend to this display"
