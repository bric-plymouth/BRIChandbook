.. _LabResources:

Lab Resources
-------------


For information on carrying out non-invasive brain stimulation studies, see the :ref:`NIBS Manual <forms>`.


Lab plan
~~~~~~~~

.. _computers:

Computers
---------

.. figure:: /brainstim/Peripherals/computers.PNG
   :alt: Set-up of computers available in the Brainstim lab.
   :width: 350px
   :align: center

   Set-up of computers available in the Brainstim lab. Slider on the middle of both computer set ups to allow participant privacy when conducting a task.


Power / switches
----------------

When using the Neuronavigation system (see Neuronavigation section below), there are two switches to turn on and off. 

.. figure:: /brainstim/Peripherals/switches1.jpeg
   :alt: Wall switch
   :width: 350px
   :align: center

   Wall Switches 

Wall switches should remain turned OFF when not in use. Under the IMAC and Polaris there are two switches. One near its power plug and the other above as seen on the picture.

.. figure:: /brainstim/Peripherals/switches2.jpeg
   :alt: Switch to turn on/off the Polaris.
   :width: 275px
   :align: center

   Switch to turn on/off the Polaris.


Organisation of the cupboards 
------------------------------

Within the cupboards of the NIBS lab you can find the following:

**Upper cupboards:**
- Tools
- New Gelpads and Used Gelpads
- Gelpad cutters
- Substitute Brainsight Equipment
- Labelling Gun

**Lower cupboards:**
- RF Wattmeter
- Gelpads Stock
- Gel Stock
- Gloves Stock
- Substitute Brainsight Equipment
- TUS equipment (For different transducers)
- Further equipment (Such as other stimulation equipment)



.. figure:: /brainstim/Peripherals/cupboards.PNG
   :alt: Switch to turn on/off the Polaris.
   :width: 500px
   :align: center

   Cupboards.


Hydrophone tank
----------------

.. figure:: /brainstim/Peripherals/hydrophone_tank.jpeg
   :alt: Hydrophone tank.
   :width: 350px
   :align: center

   Hydrophone tank.


Neuronavigation
~~~~~~~~~~~~~~~


.. figure:: /brainstim/Peripherals/Neuronavigation.PNG
   :alt: The Neuronavigation system.
   :width: 350px
   :align: right

   The Neuronavigation system.


The neuronavigation system includes the iMac with the Brainsight software installed, and the Polaris Vicra camera. 
Accompanied with te Tracking equipment, the neuronavigation system can be used to track a participant in digital space in order to alocate specific brain regions for stimulation.
To do this, you will first need to create or open a Brainsight project.


Tracking equipment
------------------

Tracking equipment is used with Brainsight to view the participant in space, allowing alocation of the brain regions and the viewing of the depth of the stimulation. 

Brainsight project
------------------

To create a Brainsight project, please consult the **NIBS MANUAL** under :ref:`Forms and Documentation <forms>` 




TUS Equipment
~~~~~~~~~~~~~

TPO
---


The Transducer Power Output (TPO) unit powers the transducer and programs the ultrasound pulse protocol. 

View the :ref:`NIBS Manual <forms>` 1.2 for how to set up the TPO (either manually or with MATLAB).

.. figure:: /brainstim/Peripherals/TPO1.PNG
   :alt: TPO screen. The blue area is a touch screen, the black knob on the right can be used to change parameters, and the square button on the bottom right is to start the transducer.  
   :width: 350px
   :align: center

   TPO screen. The blue area is a touch screen, the black knob on the right can be used to change parameters, and the square button on the bottom right is to start the transducer.  

.. figure:: /brainstim/Peripherals/TPO2.PNG
   :alt: Back of NeuroFUS TPO where mini-USB end is attached. Attach the USB end of the cable to your computer.
   :width: 275px
   :align: center

   Back of NeuroFUS TPO where mini-USB end is attached. Attach the USB end of the cable to your computer.

Transducer
----------

The current transducer used has 4 elements. The transducer is a fragile TUS equipment that should **always be coupled to a head or under water before activating it**. When not in use, please leave it inside its cover, or on the idle station attached to the matching network.

.. figure:: /brainstim/Peripherals/transducer.jpeg
   :alt: Transducer placed on white calibration block, with the tracker.
   :width: 350px
   :align: center

   Transducer placed on white calibration block, with the tracker.


TMS Equipment
~~~~~~~~~~~~~

TMS Information will be added here



TES Equipment
~~~~~~~~~~~~~
TES Information will be added here


.. raw:: html

   <br />  <!-- Add a line break for spacing -->

Emergency
~~~~~~~~~
Numbers to call if adverse events: in case of an emergency, please contact the following numbers: ... The lab has a phone in desk 1 that can be used for any situation.

.. figure:: /brainstim/phone.jpeg
   :alt: Phone located on the desk on the left upon entry in the room.
   :width: 350px
   :align: center

   Phone located on the desk on the left upon entry in the room.

- Buttons to press on the wall: In case of an emergency, a three-buttoned alarm is located near the door. Press the red button in case of the need of assistance. Press the blue button to reset and stop the alarm. In case of need of assistance, the other departments will be alerted. 

.. figure:: /brainstim/emergency.jpg
   :alt: Buttons located on the wall, on the right upon entry in the room. In red, the alarm. In blue, the reset button. In green, the emergency door release button.
   :width: 350px
   :align: center

   Buttons located on the wall, on the right upon entry in the room. In red, the alarm. In blue, the reset button. In green, the emergency door release button.


- First aid kit: it can be found in the lab located near the window. 

.. figure:: /brainstim/first_aid.jpg
   :alt: First aid kit.
   :width: 350px
   :align: center

   First aid kit.
   
