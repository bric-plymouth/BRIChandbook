# File: docs/requirements.txt

sphinx==7.2.6
sphinx-rtd-theme==2.0.0
sphinx_inline_tabs==2023.4.21
readthedocs-sphinx-search==0.3.2