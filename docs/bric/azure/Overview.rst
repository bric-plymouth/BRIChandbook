Azure Cloud Overview
---------------------

All computing resources for BRIC are hosted in Microsoft’s Azure Cloud, charged on a “pay as you go” basis, it is therefore essential your research VM is running when required and shutdown/deallocated at all other times.

There are four key steps in accessing and using your VM.

#. Your VM needs to be activated.
#. You need to be on the UoP network directly or connected via VPN. 
#. Connect/login to your active VM.
#. When your work is complete your VM needs to be shutdown and deallocated.

The options of how to do this from Windows, Linux and Mac systems are outlined in the sections below.
At the end of this document is a summary of Azure commands for quick reference.

Virtual Machine (VM) concepts
-----------------------------

.. _VMconcepts:

Your virtual machine is provisioned with Azure Storage accounts connected as defined in your initial request to build it. Please use these storage accounts for your research data, software and documents in preference to storing data locally on the internal disk (i.e. /home).
This is important to understand because - should a problem arise with your VM that needs it to be rebuilt, all data on the internal disk will be lost.
Storing data on the internal disk also has the potential to reduce performance of the VM and will in extreme cases cause it to crash or fail to start.

Landscape Diagram
-----------------

This is a basic visual overview of how the key components in and around Azure are related to each other.

.. image:: /bric/azure/Images/BRIC-users-landscape.jpg
  :width: 800 px
  :align: center
  :alt: Azure Landscape

