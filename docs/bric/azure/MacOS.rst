Managing Your VM on MacOS
---------------------------

Start VM from Web browser
*************************

Login to the `Microsoft ARM Portal <https://portal.azure.com/#home>`_ using your preferred web browser. The credentials required will be you UoP id and password.
Once logged in, select "virtual machines" from the left hand column, you will then see your VM listed, from where it can be started/stopped

.. image:: /bric/azure/Images/ARM-Portal.png
  :width: 600
  :align: center
  :alt: Azure VMs

Note the VM status of "Stopped (deallocated)", this is always the required status when your VM is not being used.

Check the tick-box next to your VM name, then use the start or stop icon from the options across the top of the page.
When your VM status changes to running, it may take a further minute for the network to initialise before allowing remote connections.

Start VM from Mac Command Line
******************************

Pre-requisities
...............
1. Install Home Brew - Details can be found `here <https://brew.sh/>`_.
2. Install Azure client.

::

  install azure-cli

3. Upgrade Azure client.

::

  Az Upgrade

Once pre-requisities are met, login to Azure::

  az login

.. image:: /bric/azure/Images/MAC-Az-login.png
  :width: 600
  :align: center
  :alt: Azure Login


This will open a browser, select your UoP account and enter password.

.. image:: /bric/azure/Images/MAC-AZ-login-2.png
  :width: 600
  :align: center
  :alt: Azure Login


Once logged in successfully you will then see this message,

.. image:: /bric/azure/Images/MAC-AZ-login-3.png
  :width: 600
  :align: center
  :alt: Azure Login


You can now close the browser or just switch back to the terminal window. If you have more than one Azure account, they will be listed when you have logged in, as in the example below,

.. image:: /bric/azure/Images/AZ-login.png
  :width: 600
  :align: center
  :alt: AZ Login


To show all your Azure accounts (Subscriptons) use::

  az account list

.. image:: /bric/azure/Images/AZ-login-2.png
  :width: 600
  :align: center
  :alt: AZ Login


To select the BRIC subscription::

  az account set –n “UoP – Brain Research Imaging Centre”

.. image:: /bric/azure/Images/AZ-login-3.png
  :width: 600
  :align: center
  :alt: AZ Login


Once the BRIC subscription is selected you are ready to start your VM.

Start your VM
.............

From the terminal window::

  az vm start –-resource-group XXXXXXXXX –-name YYYYYYYY

Where,
XXXXXXXXX is the Azure resource group that your VM is configured under.
YYYYYYYY is the name of your VM

.. image:: /bric/azure/Images/AZ-vm-start.png
  :width: 600
  :align: center
  :alt: AZ vm start


Stop your VM from Mac command Line
**********************************

It is essential that your VM is stopped AND deallocated::

  az vm stop –-resource-group XXXXXXXXX –-name YYYYYYYY
  az vm deallocate –-resource-group XXXXXXXXX –-name YYYYYYYY

.. image:: /bric/azure/Images/AZ-vm-stop-deallocate.png
  :width: 600
  :align: center
  :alt: Azure VM stop


Connect to your VM – Command line
*********************************

From the Mac terminal window use the ssh command, with your VM’s IP address::

  ssh Researcher@your.ip.address.here


.. image:: /bric/azure/Images/VM-cmdline-login.png
  :width: 600
  :align: center
  :alt: VM login cmdline


From a terminal emulator such as Termius, connect to your running VM using the IP address. (Termius can be downloaded from the App store)

.. image:: /bric/azure/Images/Termius.png
  :width: 600
  :align: center
  :alt: Termius

If you are familiar with PuTTy on Windows, this is also an option on Mac. Details `here <https://macresearch.org/putty-for-mac/>`_

Connect to your VM – Desktop Environment
****************************************

Requires Microsoft Remote Desktop software package, available from the App store

.. image:: /bric/azure/Images/Remote-Desktop.png
  :width: 600
  :align: center
  :alt: Remote Desktop

Once installed you can configure and save connection settings,

.. image:: /bric/azure/Images/Remote-Desktop-2.png
  :width: 600
  :align: center
  :alt: Remote Desktop


Continue if a certificate warning is issued,

.. image:: /bric/azure/Images/Remote-Desktop-4.png
  :width: 400
  :align: center
  :alt: Remote Desktop


Then enter your VM login details,

.. image:: /bric/azure/Images/Remote-Desktop-3.png
  :width: 400
  :align: center
  :alt: Remote Desktop



You will then be logged into the virtual desktop of your VM,

.. image:: /bric/azure/Images/Remote-Desktop-5.png
  :width: 600
  :align: center
  :alt: Remote Desktop
