BRIC handbook
=============

This is the handbook of the 
`Brain Research and Imaging Center <https://www.plymouth.ac.uk/research/psychology/brain-research-and-imaging-centre>`_  of the university of Plymouth.

URL: https://brichandbook.readthedocs.io/en/latest/index.html


How to contribute to the BRIC handbook
--------------------------------------

Note: These steps describe how to modify the handbook in GitLab directly from your browser. 
Alternatively, you can clone the handbook Gitlab repository to your local machine, 
edit it locally using your favorite text editor and commit/push your changes to Gitlab


Create a new branch
~~~~~~~~~~~~~~~~~~~

Click on the + button `on this page <https://gitlab.com/bric-plymouth/BRIChandbook>`_ and select 'New branch'

Each branch constitutes a different version of the handbook that can be viewed on the `BRIC handbook page <https://brichandbook.readthedocs.io/en/latest/index.html>`_
by expanding the arrow next to 'latest' in the bottom left menu.

Note: it may take a few minutes before your new branch is available for viewing
    
    
Make your changes
~~~~~~~~~~~~~~~~~~~

Different sections or pages of the handbook are "coded" in the .rst files located in the "docs" folder of the Gitlab repository.


To modify an existing page of the handbook, edit the corresponding .rst file (e.g. click on the .rst file and then click the "open in Web IDE" or "Edit").
These .rst files are written using the reStructuredText markup language (see `this primer <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_ for syntax basics).
Make sure that you have selected your newly created branch in Gitlab before you edit the file.

To add a new page to the handbook:
    
1. create a new .rst file either within the "docs" folder or an appropriate subfolder 
   (e.g. `on this page <https://gitlab.com/bric-plymouth/BRIChandbook>`_, navigate to the "docs" folder or appropriate subfolder, 
   click on the + button  and select 'New file';
   Note: this can also be done from within the Web IDE editor)
2. edit the file docs/index.rst, which contains the general structure of the handbook, by adding the name of the newly .rst file at the appropriate location in index.rst 
   (including its path relative to the "docs" folder if the new file is to be located in a subfolder)

To preview your changes to a .rst file without committing them (see next step), 
select the preview tab in the Gitlab web editor. If you're editing with Web IDE, 
you can use `this .rst viewer <https://overbits.herokuapp.com/rsteditor/>`_ 
(simply copy/paste your rst code and click "View as PDF". Note that Sphinx-specific features will not be rendered)

For additional .rst formatting examples, check out the `Sphinx theme page
<https://sphinx-rtd-theme.readthedocs.io/en/stable/>`_.


Commit your changes
~~~~~~~~~~~~~~~~~~~

Committing your changes on GitLab will automatically update your branch on the BRIC handbook page. 
Committing can be done from within the Gitlab file editor ("Commit changes" button) or from within the Web IDE editor (Source control button or Ctrl+shift+G). 
You must include a description of your changes.
If asked, decline creating a merge request.

To check how your changes look, select your branch on the `BRIC handbook page <https://brichandbook.readthedocs.io/en/latest/index.html>`_ (bottom left menu) 
or simply refresh the page if you have already selected the branch.

Note: there may be a delay before your committed changes are available for viewing on the handbook page.


Publish your changes 
~~~~~~~~~~~~~~~~~~~~~

Once you are ready to make your version live, merge your branch with the master branch. 
If you just recently commited changes, a "Create merge request" button will appear `on this page <https://gitlab.com/bric-plymouth/BRIChandbook>`_).
Otherwise:

1. Select "Merge requests" in the left-hand menu list.
2. Click the "New merge request" button
3. Select your new branch as the source branch and click "Compare branches and continue".

In either case, add a title and description to your merge request. 
If you want to continue working on your branch after the merge, deselect "Delete source branch ...".
Finally, click "Create merge request".
